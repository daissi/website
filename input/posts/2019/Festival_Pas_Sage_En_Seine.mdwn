[[!meta title="Festival Pas Sage En Seine (PSES) 2019"]]

L'association Debian France tiendra un stand à l'occasion du [Festival Pas Sage En Seine 2019](https://passageenseine.fr/), du 27 au 30 Juin 2019 à la Médiathèque de Choisy-le-Roi (94600) à 5 min à pied de la Gare de RER « Choisy-le-Roi » sur la ligne C.

Venez nous rencontrer afin d'échanger au sujet de la distribution [Debian](https://www.debian.org/) et de l'association [Debian France](https://france.debian.net/).

À très bientôt !
