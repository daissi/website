## Goodies

1€ = 1,2327 CHF

  - Stickers =	1 €;	CHF 1.23
  - Caps =	8 €;	CHF 10,00
  - Mouse pad =	10 €;	CHF 13,00
  - Buff =	18 €;	CHF 23,00
  - Black Polo =	25 €;	CHF 31,00
  - Blue Rugby Polo Style =	40 €;	CHF 50,00
  - Hoodies White =	35 €;	CHF 44,00
  - Hoodies black =	35 €;	CHF 44,00
  - Hoodies Grey =	35 €;	CHF 44,00
  - Hoodies blue =	35 €;	CHF 44,00
  - Hoodies white zip =	40 €;	CHF 50,00
  - Hoodies black zip =	40 €;	CHF 50,00
  - Hoodies Grey zip =	40 €;	CHF 50,00
  - Hoodies blue zip =	40 €;	CHF 50,00
