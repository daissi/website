Site Internet Debian France
===========================

URL du site : [https://france.debian.net/](https://france.debian.net/)

--------------------------------------------------------------------------------

## Adresses des dépôts Git

Les deux dépôts Git (Salsa et Gitolite) ont le même contenu.

### Salsa (Gitlab Debian)

Accès en lecture publique.
La participation est possible au travers de **Issues** et des **Merge Requests**.
Il est connecté sur le canal IRC (OTFC) `#debian-france`.

+ Vue Web : https://salsa.debian.org/debian-france-team/website
+ Git https : https://salsa.debian.org/debian-france-team/website.git
+ Git ssh : git@salsa.debian.org:debian-france-team/website.git

### Gitolite (Serveur Debian France)

Seuls les membres autorisés de l'associations ont l'autorisation d'accès.

+ Git ssh : git@france.debian.net:website

--------------------------------------------------------------------------------

## Alias git pour pusher sur les deux dépôts

+ Inclure le fichier `.git-local-config` dans la configuration locale git :
  - `git config --local include.path "$PWD/.git-local-config"`
+ Push sur les deux dépôts (Gitolite et Salsa) :
  - `git push-all`

--------------------------------------------------------------------------------

## Procedure à suivre pour la mise en place du site :

Note : Cette action est prise en charge sur le serveur par un hook lors des push sur Gitolite

```bash
bin/update-wiki . <destination>
```

