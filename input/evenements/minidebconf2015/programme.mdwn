[[!meta stylesheet="evenements/minidebconf2015/local" rel="stylesheet" title="Minidebconf2015"]]

#### Samedi / Saturday

<table class="programme">
  <thead>
    <tr>
      <th>Timeslot</th>
      <th>Title</th>
      <th>Presenter</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>10:00&nbsp;-&nbsp;10:15</td>
      <td>Arrival and registration</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>10:15&nbsp;-&nbsp;10:25</td>
      <td><em><strong>Welcome talk</strong></em></td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>10:30&nbsp;-&nbsp;11:25</td>
      <td><em><strong>State of federated RTC and WebRTC in Debian</strong></em></td>
      <td>Daniel Pocock</td>
      <td>A look at the state of federated RTC and WebRTC in Debian and the wider Internet community and where we are going in 2015 and beyond. Demo of <a class="https" href="https://rtc.debian.org">https://rtc.debian.org</a> and troubleshooting techniques. Question and answer session.  If you would like to try running WebRTC before this talk, visit <a class="http" href="http://RTCQuickStart.org">the RTC Quick Start guide</a> and see <a class="http" href="http://danielpocock.com/tags/webrtc">Daniel's WebRTC blogs</a> and feel free to send any questions you have by email to <a class="mailto" href="mailto:pocock@debian.org">pocock@debian.org</a></td>
    </tr>
    <tr>
      <td>11:30&nbsp;-&nbsp;12:30</td>
      <td><em><strong>Debsources: dive into Debian source code!</strong></em></td>
      <td>Matthieu Caneill</td>
      <td>Debsources (<a class="http" href="http://sources.debian.net">http://sources.debian.net</a>) is the entrypoint to play with the source code of all Debian packages. In this presentation, we'll have a look at its infrastructure, web interface, API, and some interesting statistics we've gathered around it.</td>
    </tr>
    <tr>
      <td>12:30&nbsp;-&nbsp;14:30</td>
      <td>Lunch break</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>14:30&nbsp;-&nbsp;15:25</td>
      <td><em><strong>Premiers pas dans l'univers de Debian</em></strong></td>
      <td>Nicolas Dandrimont</td>
      <td>Créée il y a maintenant 20 ans, la distribution Debian est devenue incontournable l'écosysteme de GNU/Linux et du logiciel libre en général. Lors de cette présentation, nous ferons une visite guidée de la distribution, sa philosophie, ses contributeurs, son  fonctionnement et son écosystème (projets amont et distributions dérivées), en tentant de montrer l'importance et le rôle central de Debian dans le monde du logiciel libre aujourd'hui.</td>
    </tr>
    <tr>
      <td>15:30&nbsp;-&nbsp;16:25</td>
      <td><em><strong>Une perception de Debian</strong></em></td>
      <td>Juliette Belin</td>
      <td>Retour d'expérience sur la création du thème graphique "Lines", thème par défaut de Jessie dont je suis l'auteur, et réflexion sur l'impact du graphisme sur l'user expérience.</td>
    </tr>
    <tr>
      <td>16:30&nbsp;-&nbsp;17:00</td>
      <td>Coffee break</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>17:00&nbsp;-&nbsp;17:55</td>
      <td><em><strong>Contribuer à Tails en travaillant sur Debian</strong></em></td>
      <td>intrigeri</td>
      <td><a class="https" href="https://tails.boum.org/">Tails</a> est devenu le système d'exploitation de référence pour quiconque chérit son anonymat, pseudonymat et/ou sa confidentialité. Tails ne pourrait pas exister sans Debian. Cette présentation montrera de nombreux moyens de participer à Tails... en contribuant à Debian.</td>
    </tr>
    <tr>
      <td>18:00&nbsp;-&nbsp;18:55</td>
      <td>Social time &amp; GPG key signing</td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>

#### Dimanche / Sunday

<table class="programme">
  <thead>
    <tr>
      <th>Timeslot</th>
      <th>Title</th>
      <th>Presenter</th>
      <th>Description</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td>10:00&nbsp;-&nbsp;10:55</td>
      <td><em><strong>GNOME for system administrators, jessie edition</strong></em></td>
      <td>Josselin Mouette</td>
      <td>GNOME, the default environment for Debian, is well-known for being a nice and usable environment for the average user. It is also designed with powerful administration features and tight integration to Debian packaging. This talk will detail how the internal plumbing behind GNOME and freedesktop technologies work and how to take advantage of it, especially in a multiple-user, multiple-machine environment.</td>
    </tr>
    <tr>
      <td>11:00&nbsp;-&nbsp;11:55</td>
      <td><em><strong>SIDUS (Single-Instance Distributing Universal System)</strong></em></td>
      <td>Emmanuel Quémener</td>
      <td>SIDUS (Single-Instance Distributing Universal System) was developed at Centre Blaise Pascal (Ecole normale supérieure de Lyon, Lyon, France), where one administrator alone is in charge of 180 stations. Emmanuel Quemener started SIDUS in February 2010, and he significantly cut his workload for administering this park of stations. SIDUS is now in use at the supercomputing centre PSMN (Pôle Scientifique de Modélisation Numérique) of the Ecole normale supérieure de Lyon. With SIDUS, you can provide a new user with a complete functional environment in just a few seconds. You can probe corrupted computers without disassembling anything. You can test new equipment without installing an OS on them. You can make your life so much easier when managing hundreds of cluster nodes, of workstations or of self-service stations. You drastically can reduce the amount of storage needed for the OS on these machines.</td>
    </tr>
    <tr>
      <td>12:00&nbsp;-&nbsp;13:55</td>
      <td>Lunch break</td>
      <td></td>
      <td></td>
    </tr>
    <tr>
      <td>14:00&nbsp;-&nbsp;14:55</td>
      <td><em><strong>Packaging the free software web</strong></em></td>
      <td>Antonio Terceiro</td>
      <td>While distributions have been excelent at providing packages, most web applications -- and server-side applications in general -- are actually composed of more than one package and perhaps non-trivial cross-package configuration that arguably does not belong at any of the individual packages. In this talk we will explore how we can have a package manager for web/server-side applications that operates one layer above out traditinal package management infrastructure. The end goal is for end users looking to self host their tools to be able to just "install an email server", "install a blog application" without having to deal with all the details themselves.</td>
    </tr>
    <tr>
      <td>15:00&nbsp;-&nbsp;15:55</td>
      <td><em><strong>Debian Long Term Support, the story of an unusual team</strong></em></td>
      <td>Raphaël Hertzog</td>
      <td>Almost anybody will acknowledge that maintaining 18000 software packages secure over 5 years is a challenge and even more so in the context of Debian where most volunteers tend to skip the parts that are not fun. Still the story of the Debian LTS team shows that it is possible. This talk will explain how we got started and where we are today.</td>
    </tr>
    <tr>
      <td>16:00&nbsp;-&nbsp;16:55</td>
      <td><em><strong>Lightning talks</strong></em></td>
      <td></td>
      <td></td>
    </tr>
  </tbody>
</table>
