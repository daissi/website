﻿Subject: Mandat de représentation aux AG Debian France

[ Instructions d'usage:
  1. Copiez-collez ce mandat dans votre logiciel de messagerie (sans les
     instructions que vous lisez actuellement).
  2. Complétez les champs vierges.
  3. Envoyez le message à bureau@france.debian.net en signant le message
     avec votre clé PGP/GPG personnelle (qui doit préalablement
     avoir été signée par un membre du CA)
]

Je soussigné, Monsieur (ou Madame) .............................,
adhérent à jour de cotisation l'association Debian France, demeurant au
.......................................................................
.......................................................................
donne pouvoir et mandate Monsieur (ou Madame) .........................
................. pour le représenter au cours des deux assemblées
générales (ordinaires et extraordinaires) du 18 janvier 2014 et
participer aux votes en son nom selon l'ordre du jour de la convocation
et conformément aux statuts et au règlement intérieur.


Fait à .................., le ../../....

Le Mandant :
Bon pour pouvoir (via signature électronique PGP/GPG)

Le Mandataire (personne qui est mandatée) :
[ Recopiez « Bon pour acceptation de pouvoir » et signez ]
|
|
|
|
|
|
|
\________________________________________________________________________
