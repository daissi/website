[[!meta title="En route pour le nouveau logo Debian France"]]

Debian France choisit son nouveau logo
======================================

Le CA de Debian France est heureux de vous annoncer l'ouverture de
l'événement "Logo Debian France".

Son objectif est de déterminer le nouveau logo officiel de Debian
France. Cet événement est encadré par les règles suivantes:

Règles encadrant les propositions pour le nouveau logo:
-------------------------------------------------------
* Toute personne peut proposer un nouveau logo en format svg (Scalable Vector Graphics) exclusivement ;
* Les propositions doivent être soumises à asso@france.debian.net avant le 31/12/2013, 00H00 +1GMT ;
* Le CA se réserve le droit de refuser les logos ne correspondant pas à l'image de Debian.

Règles encadrant le vote:
-------------------------
* L'annonce d'ouverture des votes sera faite sur la liste asso@france.debian.net au moins 15 jours avant la clôture du vote ;
* Les membres de l'association Debian France (à jour de leur cotisation) pourront voter ;
* Clôture des votes: samedi 18/01/2014, 20H +1GMT.

Règles encadrant les droits d'auteur du logo lauréat:
-----------------------------------------------------
* La licence du logo Debian France est identique à celle des logos Debian, c'est à-dire LGPL 3+ et CC-BY-SA 3.0 ;
* Le nom (ou pseudo) de l'auteur sera indiqué sur le site de l'association ;
* Pour fêter symboliquement l'ouverture de la boutique Debian France, l'artiste lauréat remportera un lot Debian France (1 polo + 1 casquette + 5 autocollants).

Annonce du résultat des votes:
------------------------------
* L'annonce du résultat du vote sera réalisée à la clôture de la mini DebConf 2014 le 19 janvier 2014 par le président de Debian France.
* L'annonce sera immédiatement retransmise sur la mailing-list asso@france.debian.net.

A vous de jouer et n'hésitez pas à diffuser cette annonce.

Nous restons à votre disposition pour toute information complémentaire.
