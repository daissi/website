[[!meta title="Mini-DebConf � Toulouse les 18 et 19 novembre 2017"]]

<img src="/posts/2017/mini-debconf/logo.png">

Bonjour � tous,

L'association Debian France [1] organise une mini-DebConf � Toulouse le week end prochain 18 et 19 novembre. Cet �v�nement accueillera toutes les personnes int�ress�es par le projet Debian avec une s�rie de conf�rences autour de ce th�me. Cette mini-DebConf aura lieu en m�me temps et au m�me endroit que le Capitole du Libre [2]  � l'ENSEEIHT : 26 rue Pierre-Paul Riquet � Toulouse.

Au programme�:

  * Premiers pas dans l'univers de Debian -- Nicolas Dandrimont
  * Trusting your computer and system -- Jonas Smedegaard
  * Automatiser la gestion de configuration de Debian avec Ansible -- Gr�gory Colpart & J�r�my Lecour
  * Making Debian for everybody -- Samuel Thibault
  * Retour d'exp�rience : mise � jour de milliers de terminaux Debian -- Cyril Brulebois
  * Retour d'exp�rience: Utilisation de Debian chez Evolix -- Evolix
  * Key signing party
  * Cocktail avec buffet le samedi soir

Le planning d�taill� est disponible [ici](https://wiki.debian.org/DebianEvents/fr/2017/Toulouse)

Vous pouvez proposer un lightning talk de 10 minutes, il n'est pas trop tard (4 talks possibles sur ce segment)

Bonne semaine et � tr�s bient�t!

Denis Briand

 * 1 [https://france.debian.net/](https://france.debian.net)
 * 2 [https://2017.capitoledulibre.org/](https://2017.capitoledulibre.org/)
