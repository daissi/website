[[!meta title="Mini Debconf 2014"]]

# Evénements - Events

## Mini Debian Conference

 The Debian France Association is organizing the third Paris Mini-DebConf. This event will welcome everyone with an interest in the Debian project for a series of talks and workshops about Debian. Allowing users, contributors and developers to meet each other is also one of the main goals of this event. We are especially looking forward to meeting Debian developers from around the world. 

The Paris Mini-DebConf 2014 will be held at Télécom ParisTech, Paris, France, on January 18 and 19.


## Schedule

<table border="1">
<tr><th>Saturday 18/01</th><td>Speaker</td><td>Title</td></tr>
<tr><td>9:30 - 10:15</td><td>Organizers</td><td>Welcome</td></tr>
<tr><td>10:15 - 11:00</td><td>Lucas Nussbaum (Debian Project Leader)</td><td>Debian for new comers</td></tr>
<tr><td>11:00 - 11:45</td><td>Laurent Séguin (AFUL)</td><td>Debian, vecteur de pérennité (in French)</td></tr>
<tr><td colspan="3">Break		</td></tr>		
<tr><td>13:30 - 13:45</td><td>Retardataires</td><td>Paiement des cotisations (in French)</td></tr>
<tr><td>14:00 - 14:30</td><td>Debian France</td><td>Vote des statuts de l'association (in French)</td></tr>
<tr><td>14:30 - 15:15</td><td>Andreas Tille</td><td>How to attract new developers for your team</td></tr>
<tr><td>15:15 - 16:00</td><td>Julien Cristau</td><td>Release team: Jessie</td></tr>
<tr><td>16:00 - 16:45</td><td>Mehdi Dogguy & Jerome Vouillon</td><td>Co-migrate</td></tr>
<tr><td>16:45 - 17:30</td><td>Antonio Terceiro</td><td>Bits from the Debian Ruby team </td></tr>
<tr><th>Sunday 19/01</th><td>Speaker</td><td>Title</td></tr>
<tr><td>9:30 - 10:15</td><td>Wookey</td><td>arm64</td></tr>
<tr><td>10:15 - 11:00</td><td>Sylvestre Ledru & Léo Cavaillé</td><td>Debile Project</td></tr>
<tr><td>11:00 - 11:45</td><td>Stephen Kitt </td><td>Cross-porting to Windows with Debian</td></tr>
<tr><td>11:45 - 12:30</td><td>Round table </td><td>Compiler selection</td></tr>
<tr><td colspan="3">Pause</td></tr>
<tr><td>13:00 - 13:45</td><td>All</td><td>Key signing party</td></tr>
<tr><td>13:45 - 14:30</td><td>Bill Allombert</td><td>Popcon the Debian popularity contest 15th birthday </td></tr>
<tr><td>14:30 - 15:15</td><td>Daniel Pocock</td><td>A federated SIP service for the Debian.org developer community </td></tr>
<tr><td>15:15 - 16:00</td><td>Stefano Zacchiroli</td><td>Debsources, powering sources.debian.net</td></tr>
<tr><td>16:00 - 16:45</td><td>David Douard</td><td>Salt to administrate Debian systems</td></tr>
<tr><td>16:45 - 17:00</td><td>Lightning talks</td><td>Cyril Brulebois - Debian-Installer<br />
Nicolas Dandrimont - Fedmsg<br />
Lucas Nussbaum - Two ways to know when your package will get removed</td></tr>
<tr><td>17:00 - 17:10</td><td>Raphaël Hertzog</td><td>Announce of the new logo of the association</td></tr>
<tr><td>17:10 - 17:15</td><td>Organizers</td><td>Bye bye</td></tr>
</tr></table>

## Location


Télécom ParisTech, 46 Rue Barrault, 75013 Paris, France

[Venue on Openstreet Map](http://www.openstreetmap.org/#map=19/48.82649/2.34650)

Entrance: 49 rue Vergniaud

Saturday: Room Estaunié

Sunday: Room B312


## Getting there

* Subway: line 6 “Corvisart“ station (5 min walk)


See the [official access map](http://www.telecom-paristech.fr/eng/practical-information/getting-there.html#c2489) for more information.

## Register

For logistic aspects, please [register on the Wiki page](https://wiki.debconf.org/wiki/Miniconf-Paris/2014). This is mandatory.
 
## Association

The status of Debian France must be updated to upgrade the association to the French 1901 law and also to update the status to make sure that Debian France will be recognized as a trusted organization.

So, a vote will occur during the event, likely Saturday afternoon.

## Sponsors
<a href="http://www.mobiskill-partner.com/"><img src="/evenements/logos/logo-mobiskill.jpg" title="Mobiskill" name="Mobiskill" border="0"/></a><br />
<a href="http://www.logilab.fr/"><img src="/evenements/logos/logo-logilab.jpg" title="Logilab" name="Logilab" border="0"/></a><br />
<a href="http://www.evolix.fr/"><img src="/evenements/logos/logo-evolix.jpg" title="Evolix" name="Evolix" border="0"/></a><br />

Sponsors are welcome. Just send an email to <<ca@france.debian.net>>.


## Organizers

* Sylvestre Ledru - <<sylvestre@debian.org>>
* Alexandre Delanoe - <<debian@delanoe.org>>
