[[!meta title="Meetup du 28 Mars � Paris"]]

Meetup du 28 Mars � Paris
=========================

## Informations pratiques

Un meetup Debian France aura lieu � Paris le mardi 28 mars 2017 � partir de 19h30.

Le meetup est accueilli par [l'Institut des Syst�mes Complexes de Paris �le de France (CNRS)](https://iscpif.fr)
, 113 rue Nationale, Paris 13�me (m�tro Nationale, Place d'Italie ou Olympiades).

[Plus d'informations pour s'y rendre](https://iscpif.fr/contact/).

Les codes � l'entr�e seront indiqu�s 24H avant le
meetup (ou par mail pour ceux qui seront inscrits):

 * code de la porte d'entr�e : XXXX
 * code de la seconde porte :  XXXX
 * Salle de conf�rence 1.1 au premier �tage (escalier ou ascenseur).

Merci de [s'inscrire](https://wiki.debian.org/DebianEvents/fr/2017/Meetup1) pour
que nous puissions pr�voir votre accueil dans les meilleures conditions.

Pour toute question concernant l'organisation, vous pouvez contacter
Alexandre Delano� (anoe AT debian DOT org).

## Programme

### Accueil   19H30-20H00
- Accueil et pr�sentation des objectifs des meetups (Alexandre Delano�, secr�taire Debian France, 5mn)
- Pr�sentation de Debian France par son Pr�sident (Nicolas Dandrimont, D�veloppeur Debian, 5mn)

### Pr�sentation (rapide) de soi, si la salle n'est pas trop pleine

### Talk 20H- 20H15
_Titre_: Debian aujourd'hui et demain

_Auteur_: Mehdi Doguy, actuel Debian Project Leader 

_R�sum�_: L'actuel Leader du projet Debian pr�sentera les moments
forts durant l'ann�e �coul�e. Il nous parlera du futur de Debian, la tendance
actuelle qui se dessine et ses projets pour l'ann�e � venir.

### Discussions 20H15-20H30

### Talk 20H30-20H45
_Titre_: Debian, forces et limites pour l'accessibilit� du logiciel libre

_Auteur_: Alexandre Arnaud, chef de projet � Hypra.fr

_R�sum�_: Comment utiliser un ordinateur lorsque l'on est mal-voyant? 
Quel syst�me pr�voir ? Alexandre Arnaud, chef de projet �
Hypra.fr montrera comment il travaille et pr�sentera l'�quipe Debian
accessibilit�, son cycle de d�veloppement favorable aux tests et sa
politique de mises � jour. Cependant, les logiciels d'aide technique
ne sont pas forc�ment � jour et certaines �volutions obligent Hypra
� maintenir ses propres d�p�ts logiciels (notamment pour certains
paquets de Mate ou Orca) afin de concilier cette stabilit� avec le
progr�s n�cessaire et rapide dans l'accessibilit� du libre.

### Discussions 20H45-21H

### Talk 21H-21H15
_Titre_: Th�mes de bureau pour Debian

_Auteur_: Aur�lien Couderc, Contributeur Debian

_R�sum�_: 
    Organisation du concours de th�mes, s�lection du th�me par d�faut et mise en �uvre.
    Challenges et travail � r�aliser � chaque version de Debian.
    Avec une mise en perspective sur les nouveaut�s pour la prochaine version de Debian : Stretch.

### Discussions 21H15-21H30

### �changes et signatures de clefs GPG 21H30-22H 

RDV pour le prochain Meetup.

