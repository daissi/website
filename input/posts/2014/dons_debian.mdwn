[[!meta title="Dons Debian France"]]

Dons Debian France
==================

En cette fin d'ann�e, nous tenons � rappeler que l'association Debian France est reconnue d'int�r�t g�n�ral. Ainsi, les donations � destination de l'association peuvent �tre d�duites des imp�ts.

Une fois la donation r�alis�e, pour obtenir un re�u, il suffit d'envoyer un mail au tr�sorier - <<tresorier@france.debian.net>> en indiquant votre adresse.

<a href="https://france.debian.net/galette/plugins/paypal/form">Formulaire de donation</a>

